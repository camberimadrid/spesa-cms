import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import "typeface-rubik";

import * as serviceWorker from "./serviceWorker";
import { Authenticator, CMSApp } from "@camberi/firecms";
import { donorView, firebaseConfig, receiverView } from "./site_config_spesa";
import SpesaMap from "./spesa_map/SpesaMap";
import logo from "./images/logo.49dd8e5d.svg";
import { firestore } from "firebase/app";

const additionalViews = [
    {
        path: "map",
        name: "MAP",
        view: <SpesaMap/>
    }
];

const authenticator: Authenticator = (user) => {

    if (user?.email?.endsWith("camberi.com"))
        return true;

    if (user?.email) {
        return firestore().collection("admins")
            .doc(user?.email)
            .get()
            .then((doc) => doc.exists) as Promise<boolean>;
    } else {
        return false;
    }
};
ReactDOM.render(
    <CMSApp
        name={"Spesasospesa"}
        authentication={authenticator}
        navigation={[
            donorView,
            receiverView
        ]}
        logo={logo}
        firebaseConfig={firebaseConfig}
        additionalViews={additionalViews}
    />,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.register();
serviceWorker.unregister();
