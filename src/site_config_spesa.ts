import {
    AlgoliaTextSearchDelegate,
    buildCollection, buildProperties,
    buildSchema,
    EnumValues,
    FilterValues,
    Properties
} from "@camberi/firecms";
import algoliasearch from "algoliasearch";

export const firebaseConfig = {
    apiKey: "AIzaSyAvw0xPb4U9xcP4RsZ0Ilc0hZlE_w5cjoU",
    authDomain: "spesa-sospesa.firebaseapp.com",
    databaseURL: "https://spesa-sospesa.firebaseio.com",
    projectId: "spesa-sospesa",
    storageBucket: "spesa-sospesa.appspot.com",
    messagingSenderId: "660585790132",
    appId: "1:660585790132:web:75af4b99d566ff739df5e1",
    measurementId: "G-CY09BQZHJX"
};

let algoliaClient: any;
if (process.env.REACT_APP_ALGOLIA_APP_ID && process.env.REACT_APP_ALGOLIA_SEARCH_KEY) {
    algoliaClient = algoliasearch(process.env.REACT_APP_ALGOLIA_APP_ID, process.env.REACT_APP_ALGOLIA_SEARCH_KEY);
} else {
    console.error("ALGOLIA_APP_ID or SEARCH_KEY env variables not specified");
    console.error("Text search not enabled");
}

const familySize: EnumValues<string> = {
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "4+": "4+"
};

const children: EnumValues<string> = {
    "0": "No",
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4+"
};

const status: EnumValues<string> = {
    "new": "New",
    "done": "Done",
    "rejected": "Rejected",
    "waiting_confirmation": "Waiting for confirmation",
    "too_far": "Too far"
};

const foodType: EnumValues<string> = {
    "normal": "Normal",
    "vegetarian": "Vegetarian",
    "halal": "Halal"
};

export const statusFilter: FilterValues<any> = { "status": ["in", ["new", "waiting_confirmation", "brigade"]] };

const donorProperties: Properties = buildProperties({
    name: {
        title: "Name",
        dataType: "string",
        disabled: true
    },
    added_on: {
        title: "Added on",
        dataType: "timestamp",
        disabled: true
    },
    status: {
        title: "Status",
        dataType: "string",
        config: {
            enumValues: status
        },
        description: "Changing the status here does NOT update the receiver"
    },
    phone: {
        title: "Phone number",
        dataType: "string"
    },
    amount: {
        title: "Amount",
        dataType: "number"
    },
    message: {
        title: "Message",
        dataType: "string",
        config:{
            multiline: true
        }
    },
    additional: {
        title: "Additional",
        dataType: "string",
        config:{
            multiline: true
        }
    },
});

export const receiverSchema = buildSchema({
    name: "Receiver",
    properties: {
        name: {
            title: "Name",
            dataType: "string"
        },
        status: {
            dataType: "string",
            title: "Status",
            config:{
                enumValues: status,
            }
        },
        added_on: {
            title: "Added on",
            dataType: "timestamp",
            disabled: true
        },
        phone: {
            title: "Phone number",
            dataType: "string"
        },
        address: {
            title: "Address",
            dataType: "string"
        },
        food_type: {
            title: "Food type",
            dataType: "string",
            config: {
                enumValues: foodType
            }
        },
        family_size: {
            title: "Family size",
            dataType: "string",
            config: {
                enumValues: familySize
            }
        },
        children_count: {
            title: "Kids",
            dataType: "string",
            config: {
                enumValues: children
            }
        },
        allergies: {
            title: "Allergies",
            dataType: "string"
        },
        additional: {
            title: "Additional",
            dataType: "string",
            config:{
                multiline: true
            }
        },

    }
});

export const receiverView = buildCollection({
    relativePath: "cities/milano/receivers",
    schema: receiverSchema,
    textSearchDelegate: new AlgoliaTextSearchDelegate(
        algoliaClient,
        "milano_receivers"),
    filterableProperties: ["status"],
    pagination: false,
    group: "Milano",
    name: "Receivers"
});

export const donorView = buildCollection({
    relativePath: "donors",
    schema: {
        name: "Donor",
        properties: donorProperties
    },
    textSearchDelegate: new AlgoliaTextSearchDelegate(
        algoliaClient,
        "donors"),
    pagination: false,
    name: "Donors"
});

