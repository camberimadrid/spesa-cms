import React, { useEffect } from "react";
import { Box, Table, TableCell, TableRow } from "@material-ui/core";
import * as firebase from "firebase";
import "firebase/firestore";
import Typography from "@material-ui/core/Typography";

export async function generateStatsCity(firestore: firebase.firestore.Firestore, city: string) {
    let res: any = {};
    await firestore.collection(`cities/${city}/carriers`)
        .get()
        .then((snapshot) => {
            const docs = snapshot.docs.filter(d => d.get("status") !== "rejected");
            const carriersTotalAmount = docs.map(d => parseInt(d.get("amount"))).reduce((a, b) => a + b);
            res["carriersCount"] = docs.length;
            res["carriersTotalAmount"] = carriersTotalAmount;
        });

    await firestore.collection(`cities/${city}/receivers`)
        .get()
        .then((snapshot) => {
            const docs = snapshot.docs.filter(d => d.get("status") !== "rejected");
            res["matchesCount"] = docs.filter(d => d.get("carrier_match")).length;
            res["receiversCount"] = docs.length;
        });
    return res;
}

export async function generateStatsDonors(firestore: firebase.firestore.Firestore) {
    let res: any = {};
    await firestore.collection("donors")
        .get()
        .then((snapshot) => {
            const donorsTotalAmount = snapshot.docs.map(d => parseInt(d.get("amount"))).reduce((a, b) => a + b);
            res["donorsCount"] = snapshot.docs.length;
            res["donorsTotalAmount"] = donorsTotalAmount;
        });
    return res;
}

export default function Stats() {
    const [donorStats, setDonor] = React.useState<any | null>();
    const [milanoStats, setMilano] = React.useState<any | null>();

    useEffect(() => {
        generateStatsCity(firebase.firestore(), "milano").then((cityStats) => setMilano(cityStats));
        generateStatsDonors(firebase.firestore()).then((donorStats) => setDonor(donorStats));
    }, []);
    return <>
        <Box style={{ position: "relative" }}>
            <Box bgcolor={"white"} p={3}>
                <Typography variant="h5">Stats</Typography>
            </Box>
        </Box>

        <Table>

            {donorStats && <>
                <TableRow>
                    <TableCell component="th" scope="row">
                        Donors count
                    </TableCell>
                    <TableCell
                        align="right">{donorStats.donorsCount}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell component="th" scope="row">
                        Donors total euros
                    </TableCell>
                    <TableCell
                        align="right">{donorStats.donorsTotalAmount}</TableCell>
                </TableRow>
            </>}

            {milanoStats && <>
                <TableRow>
                    <TableCell component="th" scope="row">
                        Milano carriers count
                    </TableCell>
                    <TableCell
                        align="right">{milanoStats.carriersCount}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell component="th" scope="row">
                        Milano carriers total amount
                    </TableCell>
                    <TableCell
                        align="right">{milanoStats.carriersTotalAmount}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell component="th" scope="row">
                        Milano matches count
                    </TableCell>
                    <TableCell
                        align="right">{milanoStats.matchesCount}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell component="th" scope="row">
                        Milano receivers count
                    </TableCell>
                    <TableCell
                        align="right">{milanoStats.receiversCount}</TableCell>
                </TableRow>
            </>}

        </Table>
    </>;
}