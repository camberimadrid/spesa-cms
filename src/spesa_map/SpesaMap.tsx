import React, { useEffect } from "react";
import { Entity, FilterValues, listenCollection } from "@camberi/firecms";
import { receiverSchema } from "../site_config_spesa";
import {
    GoogleMap,
    InfoWindow,
    LoadScript,
    Marker
} from "@react-google-maps/api";
import PinReceiver from "../images/pin_receiver.png";
import PinReceiverInactive from "../images/pin_receiver_inactive.png";
import { createStyles, Theme } from "@material-ui/core";
import { MapStyle } from "./SpesaMapStyle";
import { makeStyles } from "@material-ui/core/styles";

interface SpesaMapProps {}

export const statusFilter: FilterValues<any> = { status: ["in", ["new"]] };

export const mapStyles = makeStyles((theme: Theme) =>
    createStyles({
        map: {
            width: "calc(100vw - 240px)",
            left: 240,
            position: "fixed",
            height: "calc(100vh - 64px)",
            top: 64,
        },
        infoWindow: {
            lineHeight: 1.4,
        },
        statusLabel: {
            textTransform: "uppercase",
            fontSize: 10,
        },
        title: {
            margin: 0,
            marginBottom: 5,
        },
        boxes: {
            boxShadow: "0px 0px 10px rgba(0,0,0,0.3)",
        },
    })
);

const centerValues = { lat: 45.49463, lng: 9.22131 };

const distance = 0.00005;

export default function SpesaMap({}: SpesaMapProps) {
    const [receivers, setReceivers] = React.useState<Entity<typeof receiverSchema>[]>([]);

    const [selectedReceiver, setSelectedReceiver] = React.useState<Entity<typeof receiverSchema> | null>();
    const [center, setCenter] = React.useState<{ lat: number; lng: number }>(centerValues);

    const receiversPath = "cities/milano/receivers";

    useEffect(() => {
        const cancelReceiverSubscription = listenCollection(
            receiversPath,
            receiverSchema,
            (entities) => {
                setReceivers(entities);
            },
            (e) => {},
            statusFilter
        );
        return () => {
            cancelReceiverSubscription();
        };
    }, []);

    // console.log("rrr" , receivers);
    //receivers.map(r => r.values.location).forEach(console.log);

    const classes = mapStyles();

    const mapContainerStyle = {
        height: "100%",
        width: "100%",
    };

    return (
            <div className={classes.map}>
                <LoadScript id="script-loader" googleMapsApiKey="AIzaSyCPevHQ_QdwW6Y8ptG2rCtVllrAtwdXnDQ">
                    <GoogleMap
                        mapContainerStyle={mapContainerStyle}
                        zoom={14}
                        options={{
                            styles: MapStyle,
                            streetViewControl: false,
                            mapTypeControl: false,
                            fullscreenControl: false,
                        }}
                        center={center}
                        clickableIcons={false}
                    >
                        {receivers.map((receiver, key) => (
                            <Marker
                                key={`marker-receiver-${key}`}
                                title={"Receiver " + receiver.values.name}
                                clickable={true}
                                options={{
                                    icon: {
                                        url: receiver.values.status === "new" ? PinReceiver : PinReceiverInactive,
                                        // This marker is 20 pixels wide by 32 pixels high.
                                        size: new google.maps.Size(50, 66),
                                        // The origin for this image is (0, 0).
                                        origin: new google.maps.Point(0, 0),
                                        // The anchor for this image is the base of the flagpole at (0, 32).
                                        anchor: new google.maps.Point(12, 33),
                                        scaledSize: new google.maps.Size(20, 27),
                                    },
                                }}
                                onClick={() => setSelectedReceiver(receiver)}
                                position={{
                                    lat: ((receiver.values as any)["location"]).latitude,
                                    lng: ((receiver.values as any)["location"]).longitude,
                                }}
                            />
                        ))}

                        {selectedReceiver && (
                            <InfoWindow
                                onCloseClick={() => setSelectedReceiver(null)}
                                options={{ pixelOffset: new google.maps.Size(-3, -30), disableAutoPan: true }}
                                position={{
                                    lat: ((selectedReceiver.values as any)["location"]).latitude,
                                    lng: ((selectedReceiver.values as any)["location"]).longitude,
                                }}
                            >
                                <div className={classes.infoWindow}>
                                    <span className={classes.statusLabel}>Receiver</span>
                                    <h3 className={classes.title}>{selectedReceiver.values.name}</h3>
                                    <div>
                                        <strong>Tel:</strong> {selectedReceiver.values.phone}
                                    </div>
                                    <div>
                                        <strong>Add:</strong> {selectedReceiver.values.address}
                                    </div>
                                    <div>
                                        <strong>Family size:</strong> {selectedReceiver.values.family_size}
                                    </div>
                                    <div>
                                        <strong>Kids:</strong> {selectedReceiver.values.children_count}
                                    </div>
                                </div>
                            </InfoWindow>
                        )}

                    </GoogleMap>
                </LoadScript>
            </div>
    );
}
